package Assignment4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;


public class experimentsA4 {
	
	public ArrayList<Integer> alInputs;
	
	public void experiments(String filename, ArrayList<Integer> values) throws IOException
	{
		FileReader freader = new FileReader(filename);
		BufferedReader reader = new BufferedReader(freader);
		StopWatch sw = new StopWatch();
		alInputs = new ArrayList<Integer>();
		String s = reader.readLine();
		while (s != null)
		{
			try
			{
				int n = Integer.parseInt(s);
				if (n >= 0)
				{
					alInputs.add(n);
				}
				s = reader.readLine();
			}
			catch (NumberFormatException e)
			{
				s = reader.readLine();
			}
		}
		freader.close();
		reader.close();
//		/* experiment 1 - sequential search w/ ArrayList */
		int count = 0;
		int a = 0;
		int next = 0;
		System.out.println("Experiment 1:");
		sw.start();
		while (a < alInputs.size())
		{
			for (int i = 0; i < values.size(); i++)
			{
				if (alInputs.get(a).equals(values.get(i)))
				{
					System.out.println(alInputs.get(a) + " found at index " + i);
					count++;
					i = values.size();
				}
			}
			a++;
		}
		sw.stop();
		System.out.println(count + " out of " + alInputs.size() + " found");
		System.out.println(sw.getElapsedTime());
		System.out.println();
		sw.reset(); 
		/* experiment 2 - sequential search w/ LinkedList */
		LinkedList<Integer> llValues = new LinkedList<Integer>(values);
		//Iterator<Integer> valuesIter = alInputs.iterator();
		Iterator<Integer> inputs = llValues.iterator();
		count = 0;
		a = 0;
		next = 0;
		sw.start();
		System.out.println("Experiment 2:");
		while (a < alInputs.size())
		{
			for (int i = 0; i < llValues.size(); i++)
			{
				next = inputs.next();
				if (alInputs.get(a).equals(next))
				{
					System.out.println(alInputs.get(a) + " found at index " + i);
					count++;
					i = llValues.size();
				}
			}
			a++;
			inputs = llValues.iterator();
		}
		sw.stop();
		System.out.println(count + " out of " + alInputs.size() + " found");
		System.out.println(sw.getElapsedTime());
		System.out.println();
		sw.reset();
		/* experiment 3 - sort the ArrayList */
		System.out.println("Experiment 3:");
		sw.start();
		ArrayList<Integer> sortedValues = values;
		Collections.sort(sortedValues);
		sw.stop();
		System.out.println(sw.getElapsedTime() + " nanoseconds to sort");
		System.out.println();
		sw.reset();
		/* experiment 4 - binary search */
		Integer [] sValues = new Integer[sortedValues.size()];
		sortedValues.toArray(sValues);
		count = 0;
		a = 0;
		System.out.println("Experiment 4:");
		sw.start();
		while (a < alInputs.size())
		{
			int result = Arrays.binarySearch(sValues, alInputs.get(a));
			if (result >= 0)
			{
				System.out.println(alInputs.get(a) + " found at index " + result);
				count++;
			}
			a++;
		}
		sw.stop();
		System.out.println(count + " out of " + alInputs.size() + " found");
		System.out.println(sw.getElapsedTime() + " nanoseconds to sort");
		System.out.println();
		sw.reset();
		/* experiment 5 - interpolation search */
		count = 0;
		a = 0;
		int low = 0;
		int high = sValues.length - 1;
		int mid;
		double temp,rise,run,x;
		boolean notFound = true;
		System.out.println("Experiment 5:");
		sw.start();
		while (a < alInputs.size())
		{
			while (sValues[low] <= alInputs.get(a) && sValues[high] >= alInputs.get(a) && notFound)
			{
				rise = high - low;
				run = sValues[high] - sValues[low];
				temp = rise/run;
				x = alInputs.get(a) - sValues[low];
				temp = temp*x;
				temp = Math.ceil(temp);
				//temp = Math.ceil(((alInputs.get(a) - sValues[low]) * (high-low)/(sValues[high]-sValues[low])));
				mid = (int)(low + temp);
				if (sValues[mid].compareTo(alInputs.get(a)) < 0)
				{
					low = mid + 1;
				}
				else if (sValues[mid].compareTo(alInputs.get(a)) > 0)
				{
					high = mid - 1;
				}
				else
				{
					System.out.println(alInputs.get(a) + " found at index " + mid);
					count++;
					notFound = false;
				}
			}
			low = 0;
			high = sValues.length - 1;
			notFound = true;
			a++;
		}
		sw.stop();
		System.out.println(count + " out of " + alInputs.size() + " found");
		System.out.println(sw.getElapsedTime() + " nanoseconds to sort");
		System.out.println();
		sw.reset();
	}

}
