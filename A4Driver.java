package Assignment4;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class A4Driver {
	
	public static void main(String[] args) throws IOException
	{
		if (args.length < 1)
		{
			System.out.println("error");
		}
		Random r = new Random(15485863);
		ArrayList<Integer> values = new ArrayList<Integer>();
		for(int i = 0; i < 1000000; i++)
		{
			values.add(r.nextInt(Integer.MAX_VALUE));
		}
		experimentsA4 exps = new experimentsA4();
		exps.experiments(args[0],values);
	}
}

